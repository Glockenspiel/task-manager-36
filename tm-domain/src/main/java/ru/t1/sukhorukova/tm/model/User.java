package ru.t1.sukhorukova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.enumerated.Role;

@Setter
@Getter
@NoArgsConstructor
public class User extends AbstractModel {

    @Nullable
    private String login;

    @Nullable
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @NotNull
    private Role role = Role.USUAL;

    @NotNull
    private Boolean locked = false;

    public User(
            @NotNull final String login,
            @NotNull final String passwordHash
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public User(
            @NotNull final String login,
            @NotNull final String passwordHash,
            @NotNull final String email
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

    public User(
            @NotNull final String login,
            @NotNull final String passwordHash,
            @NotNull final Role role
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
    }

}
