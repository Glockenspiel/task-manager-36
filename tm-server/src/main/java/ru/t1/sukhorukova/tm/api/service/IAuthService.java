package ru.t1.sukhorukova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.model.Session;

public interface IAuthService {

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    @NotNull
    Session validateToken(@Nullable String token);

    void logout(@Nullable final String token);

}
