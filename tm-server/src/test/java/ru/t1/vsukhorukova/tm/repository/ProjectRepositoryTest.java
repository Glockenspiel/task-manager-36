package ru.t1.vsukhorukova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sukhorukova.tm.api.repository.IProjectRepository;
import ru.t1.sukhorukova.tm.enumerated.ProjectSort;
import ru.t1.sukhorukova.tm.model.Project;
import ru.t1.sukhorukova.tm.repository.ProjectRepository;
import ru.t1.vsukhorukova.tm.marker.UnitCategory;

import java.util.Comparator;
import java.util.List;

import static ru.t1.vsukhorukova.tm.constant.ProjectTestData.*;
import static ru.t1.vsukhorukova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @Test
    public void testProjectAdd() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, repository.findAll().get(0));
    }

    @Test
    public void testProjectAddByUserId() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1.getId(), USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, repository.findAll().get(0));
    }

    @Test
    public void testProjectAddModels() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(ADMIN_PROJECT_LIST);
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(0), repository.findAll().get(0));
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(1), repository.findAll().get(1));
    }

    @Test
    public void testProjectSet() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(ADMIN_PROJECT_LIST);
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(0), repository.findAll().get(0));
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(1), repository.findAll().get(1));
        repository.set(USER2_PROJECT_LIST);
        Assert.assertEquals(USER2_PROJECT_LIST.get(0), repository.findAll().get(0));
    }

    @Test
    public void testProjectFindAll() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_PROJECT_LIST);
        @NotNull final List<Project> projectList = repository.findAll();
        Assert.assertEquals(USER1_PROJECT_LIST.get(0), projectList.get(0));
        Assert.assertEquals(USER1_PROJECT_LIST.get(1), projectList.get(1));
        Assert.assertEquals(USER1_PROJECT_LIST.get(2), projectList.get(2));
    }

    @Test
    public void testProjectFindAllByUserId() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_PROJECT_LIST);
        repository.add(ADMIN_PROJECT_LIST);
        @NotNull final List<Project> projectListUser1 = repository.findAll(USER1.getId());
        Assert.assertEquals(USER1_PROJECT_LIST.get(0), projectListUser1.get(0));
        Assert.assertEquals(USER1_PROJECT_LIST.get(1), projectListUser1.get(1));
        Assert.assertEquals(USER1_PROJECT_LIST.get(2), projectListUser1.get(2));
        @NotNull final List<Project> projectListAdmin = repository.findAll(ADMIN.getId());
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(0), projectListAdmin.get(0));
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(1), projectListAdmin.get(1));
    }

    @Test
    public void testProjectFindAllComparator() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_PROJECT_LIST.get(2));
        repository.add(USER1_PROJECT_LIST.get(0));
        repository.add(USER1_PROJECT_LIST.get(1));
        @NotNull final ProjectSort sort = ProjectSort.BY_NAME;
        @NotNull final Comparator<Project> comparator = sort.getComparator();
        @NotNull final List<Project> projectList = repository.findAll(comparator);
        Assert.assertEquals(USER1_PROJECT_LIST.get(0), projectList.get(0));
        Assert.assertEquals(USER1_PROJECT_LIST.get(1), projectList.get(1));
        Assert.assertEquals(USER1_PROJECT_LIST.get(2), projectList.get(2));
    }

    public void testProjectFindAllComparatorByUserId() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_PROJECT_LIST.get(2));
        repository.add(ADMIN_PROJECT_LIST.get(1));
        repository.add(USER1_PROJECT_LIST.get(0));
        repository.add(ADMIN_PROJECT_LIST.get(0));
        repository.add(USER1_PROJECT_LIST.get(1));
        @NotNull final ProjectSort sort = ProjectSort.BY_NAME;
        @NotNull final Comparator<Project> comparator = sort.getComparator();
        @NotNull final List<Project> projectListUser1 = repository.findAll(USER1.getId(), comparator);
        Assert.assertEquals(USER1_PROJECT_LIST.get(0), projectListUser1.get(0));
        Assert.assertEquals(USER1_PROJECT_LIST.get(1), projectListUser1.get(1));
        Assert.assertEquals(USER1_PROJECT_LIST.get(2), projectListUser1.get(2));
        @NotNull final List<Project> projectListAdmin = repository.findAll(ADMIN.getId(), comparator);
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(0), projectListAdmin.get(0));
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(1), projectListAdmin.get(1));
    }

    @Test
    public void testProjectFindOneById() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT2, repository.findOneById(USER1_PROJECT2.getId()));
        Assert.assertNull(repository.findOneById("test-id"));
    }

    @Test
    public void testProjectFindOneByIdByUserId() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_PROJECT_LIST);
        repository.add(ADMIN_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT2, repository.findOneById(USER1.getId(), USER1_PROJECT2.getId()));
        Assert.assertEquals(ADMIN_PROJECT2, repository.findOneById(ADMIN.getId(), ADMIN_PROJECT2.getId()));
        Assert.assertNull(repository.findOneById(USER1.getId(), ADMIN_PROJECT2.getId()));
        Assert.assertNull(repository.findOneById(null, ADMIN_PROJECT2.getId()));
        Assert.assertNull(repository.findOneById(USER1.getId(), "test-id"));
    }

    @Test
    public void testProjectFindOneByIndex() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT2, repository.findOneByIndex(1));
    }

    @Test
    public void testProjectFindOneByIndexByUserId() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_PROJECT_LIST);
        repository.add(ADMIN_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST.get(1), repository.findOneByIndex(USER1.getId(), 1));
        Assert.assertEquals(ADMIN_PROJECT_LIST.get(0), repository.findOneByIndex(ADMIN.getId(), 0));
        Assert.assertNull(repository.findOneByIndex(null, 0));
    }

    @Test
    public void testProjectRemoveOne() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_PROJECT_LIST);
        Assert.assertNotNull(repository.findOneById(USER1_PROJECT2.getId()));
        repository.removeOne(USER1_PROJECT2);
        Assert.assertNull(repository.findOneById(USER1_PROJECT2.getId()));
    }

    @Test
    public void testProjectRemoveOneByUserId() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_PROJECT_LIST);
        repository.add(USER2_PROJECT_LIST);
        Assert.assertNotNull(repository.findOneById(USER1_PROJECT2.getId()));
        repository.removeOne(USER2.getId(), USER1_PROJECT2);
        Assert.assertNotNull(repository.findOneById(USER1_PROJECT2.getId()));
        repository.removeOne(USER1.getId(), USER1_PROJECT2);
        Assert.assertNull(repository.findOneById(USER1_PROJECT2.getId()));
    }

    @Test
    public void testProjectRemoveOneById() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_PROJECT_LIST);
        Assert.assertNotNull(repository.findOneById(USER1_PROJECT2.getId()));
        repository.removeOneById(USER1_PROJECT2.getId());
        Assert.assertNull(repository.findOneById(USER1_PROJECT2.getId()));
    }

    @Test
    public void testProjectRemoveOneByIdByUserId() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_PROJECT_LIST);
        repository.add(USER2_PROJECT_LIST);
        Assert.assertNotNull(repository.findOneById(USER1_PROJECT2.getId()));
        repository.removeOneById(USER2.getId(), USER1_PROJECT2.getId());
        Assert.assertNotNull(repository.findOneById(USER1_PROJECT2.getId()));
        repository.removeOneById(USER1.getId(), USER1_PROJECT2.getId());
        Assert.assertNull(repository.findOneById(USER1_PROJECT2.getId()));
    }

    @Test
    public void testProjectRemoveOneByIndex() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_PROJECT1);
        Assert.assertNotNull(repository.findOneById(USER1_PROJECT1.getId()));
        repository.removeOneByIndex(0);
        Assert.assertNull(repository.findOneById(USER1_PROJECT1.getId()));
    }

    @Test
    public void testProjectRemoveOneByIndexByUserId() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_PROJECT1);
        repository.add(USER2_PROJECT1);
        Assert.assertNotNull(repository.findOneById(USER1_PROJECT1.getId()));
        Assert.assertNotNull(repository.findOneById(USER2_PROJECT1.getId()));
        repository.removeOneByIndex(USER1.getId(), 0);
        Assert.assertNotNull(repository.findOneById(USER2_PROJECT1.getId()));
        Assert.assertNull(repository.findOneById(USER1_PROJECT1.getId()));
    }

    @Test
    public void testProjectRemoveAll() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST.size(), repository.findAll().size());
        repository.removeAll();
        Assert.assertEquals(0, repository.findAll().size());
    }

    @Test
    public void testProjectRemoveAllByUserId() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_PROJECT_LIST);
        repository.add(USER2_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST.size(), repository.findAll(USER1.getId()).size());
        Assert.assertEquals(USER2_PROJECT_LIST.size(), repository.findAll(USER2.getId()).size());
        repository.removeAll(USER1.getId());
        Assert.assertEquals(0, repository.findAll(USER1.getId()).size());
        Assert.assertEquals(USER2_PROJECT_LIST.size(), repository.findAll(USER2.getId()).size());
    }

    @Test
    public void testProjectGetSize() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST.size(), repository.getSize());
    }

    @Test
    public void testProjectGetSizeByUserId() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_PROJECT_LIST);
        repository.add(USER2_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST.size(), repository.getSize(USER1.getId()));
        Assert.assertEquals(USER2_PROJECT_LIST.size(), repository.getSize(USER2.getId()));
    }

    @Test
    public void testProjectExistsById() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_PROJECT_LIST);
        Assert.assertTrue(repository.existsById(USER1_PROJECT1.getId()));
        Assert.assertFalse(repository.existsById(USER2_PROJECT1.getId()));
    }

    @Test
    public void testProjectExistsByIdByUserId() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_PROJECT_LIST);
        repository.add(USER2_PROJECT_LIST);
        Assert.assertTrue(repository.existsById(USER1.getId(), USER1_PROJECT1.getId()));
        Assert.assertFalse(repository.existsById(USER2.getId(), USER1_PROJECT1.getId()));
    }

}
