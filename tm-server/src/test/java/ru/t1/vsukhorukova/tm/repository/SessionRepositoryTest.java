package ru.t1.vsukhorukova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sukhorukova.tm.api.repository.ISessionRepository;
import ru.t1.sukhorukova.tm.model.Session;
import ru.t1.sukhorukova.tm.repository.SessionRepository;
import ru.t1.vsukhorukova.tm.marker.UnitCategory;

import java.util.List;

import static ru.t1.vsukhorukova.tm.constant.SessionTestData.*;
import static ru.t1.vsukhorukova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @Test
    public void testSessionAdd() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1, repository.findAll().get(0));
    }

    @Test
    public void testSessionAddByUserId() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1.getId(), USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1, repository.findAll().get(0));
    }

    @Test
    public void testSessionAddModels() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(ADMIN_SESSION_LIST);
        Assert.assertEquals(ADMIN_SESSION_LIST.get(0), repository.findAll().get(0));
        Assert.assertEquals(ADMIN_SESSION_LIST.get(1), repository.findAll().get(1));
    }

    @Test
    public void testSessionSet() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(ADMIN_SESSION_LIST);
        Assert.assertEquals(ADMIN_SESSION_LIST.get(0), repository.findAll().get(0));
        Assert.assertEquals(ADMIN_SESSION_LIST.get(1), repository.findAll().get(1));
        repository.set(USER2_SESSION_LIST);
        Assert.assertEquals(USER2_SESSION_LIST.get(0), repository.findAll().get(0));
    }

    @Test
    public void testSessionFindAll() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_SESSION_LIST);
        @NotNull final List<Session> sessionList = repository.findAll();
        Assert.assertEquals(USER1_SESSION_LIST.get(0), sessionList.get(0));
        Assert.assertEquals(USER1_SESSION_LIST.get(1), sessionList.get(1));
        Assert.assertEquals(USER1_SESSION_LIST.get(2), sessionList.get(2));
    }

    @Test
    public void testSessionFindAllByUserId() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_SESSION_LIST);
        repository.add(ADMIN_SESSION_LIST);
        @NotNull final List<Session> sessionListUser1 = repository.findAll(USER1.getId());
        Assert.assertEquals(USER1_SESSION_LIST.get(0), sessionListUser1.get(0));
        Assert.assertEquals(USER1_SESSION_LIST.get(1), sessionListUser1.get(1));
        Assert.assertEquals(USER1_SESSION_LIST.get(2), sessionListUser1.get(2));
        @NotNull final List<Session> sessionListAdmin = repository.findAll(ADMIN.getId());
        Assert.assertEquals(ADMIN_SESSION_LIST.get(0), sessionListAdmin.get(0));
        Assert.assertEquals(ADMIN_SESSION_LIST.get(1), sessionListAdmin.get(1));
    }

    @Test
    public void testSessionFindOneById() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION2, repository.findOneById(USER1_SESSION2.getId()));
        Assert.assertNull(repository.findOneById("test-id"));
    }

    @Test
    public void testSessionFindOneByIdByUserId() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_SESSION_LIST);
        repository.add(ADMIN_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION2, repository.findOneById(USER1.getId(), USER1_SESSION2.getId()));
        Assert.assertEquals(ADMIN_SESSION2, repository.findOneById(ADMIN.getId(), ADMIN_SESSION2.getId()));
        Assert.assertNull(repository.findOneById(USER1.getId(), ADMIN_SESSION2.getId()));
        Assert.assertNull(repository.findOneById(null, ADMIN_SESSION2.getId()));
        Assert.assertNull(repository.findOneById(USER1.getId(), "test-id"));
    }

    @Test
    public void testSessionFindOneByIndex() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION2, repository.findOneByIndex(1));
    }

    @Test
    public void testSessionFindOneByIndexByUserId() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_SESSION_LIST);
        repository.add(ADMIN_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST.get(1), repository.findOneByIndex(USER1.getId(), 1));
        Assert.assertEquals(ADMIN_SESSION_LIST.get(0), repository.findOneByIndex(ADMIN.getId(), 0));
        Assert.assertNull(repository.findOneByIndex(null, 0));
    }

    @Test
    public void testSessionRemoveOne() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_SESSION_LIST);
        Assert.assertNotNull(repository.findOneById(USER1_SESSION2.getId()));
        repository.removeOne(USER1_SESSION2);
        Assert.assertNull(repository.findOneById(USER1_SESSION2.getId()));
    }

    @Test
    public void testSessionRemoveOneByUserId() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_SESSION_LIST);
        repository.add(USER2_SESSION_LIST);
        Assert.assertNotNull(repository.findOneById(USER1_SESSION2.getId()));
        repository.removeOne(USER2.getId(), USER1_SESSION2);
        Assert.assertNotNull(repository.findOneById(USER1_SESSION2.getId()));
        repository.removeOne(USER1.getId(), USER1_SESSION2);
        Assert.assertNull(repository.findOneById(USER1_SESSION2.getId()));
    }

    @Test
    public void testSessionRemoveOneById() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_SESSION_LIST);
        Assert.assertNotNull(repository.findOneById(USER1_SESSION2.getId()));
        repository.removeOneById(USER1_SESSION2.getId());
        Assert.assertNull(repository.findOneById(USER1_SESSION2.getId()));
    }

    @Test
    public void testSessionRemoveOneByIdByUserId() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_SESSION_LIST);
        repository.add(USER2_SESSION_LIST);
        Assert.assertNotNull(repository.findOneById(USER1_SESSION2.getId()));
        repository.removeOneById(USER2.getId(), USER1_SESSION2.getId());
        Assert.assertNotNull(repository.findOneById(USER1_SESSION2.getId()));
        repository.removeOneById(USER1.getId(), USER1_SESSION2.getId());
        Assert.assertNull(repository.findOneById(USER1_SESSION2.getId()));
    }

    @Test
    public void testSessionRemoveOneByIndex() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_SESSION1);
        Assert.assertNotNull(repository.findOneById(USER1_SESSION1.getId()));
        repository.removeOneByIndex(0);
        Assert.assertNull(repository.findOneById(USER1_SESSION1.getId()));
    }

    @Test
    public void testSessionRemoveOneByIndexByUserId() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_SESSION1);
        repository.add(USER2_SESSION1);
        Assert.assertNotNull(repository.findOneById(USER1_SESSION1.getId()));
        Assert.assertNotNull(repository.findOneById(USER2_SESSION1.getId()));
        repository.removeOneByIndex(USER1.getId(), 0);
        Assert.assertNotNull(repository.findOneById(USER2_SESSION1.getId()));
        Assert.assertNull(repository.findOneById(USER1_SESSION1.getId()));
    }

    @Test
    public void testSessionRemoveAll() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST.size(), repository.findAll().size());
        repository.removeAll();
        Assert.assertEquals(0, repository.findAll().size());
    }

    @Test
    public void testSessionRemoveAllByUserId() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_SESSION_LIST);
        repository.add(USER2_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST.size(), repository.findAll(USER1.getId()).size());
        Assert.assertEquals(USER2_SESSION_LIST.size(), repository.findAll(USER2.getId()).size());
        repository.removeAll(USER1.getId());
        Assert.assertEquals(0, repository.findAll(USER1.getId()).size());
        Assert.assertEquals(USER2_SESSION_LIST.size(), repository.findAll(USER2.getId()).size());
    }

    @Test
    public void testSessionGetSize() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST.size(), repository.getSize());
    }

    @Test
    public void testSessionGetSizeByUserId() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_SESSION_LIST);
        repository.add(USER2_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST.size(), repository.getSize(USER1.getId()));
        Assert.assertEquals(USER2_SESSION_LIST.size(), repository.getSize(USER2.getId()));
    }

    @Test
    public void testSessionExistsById() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_SESSION_LIST);
        Assert.assertTrue(repository.existsById(USER1_SESSION1.getId()));
        Assert.assertFalse(repository.existsById(USER2_SESSION1.getId()));
    }

    @Test
    public void testSessionExistsByIdByUserId() {
        @NotNull final ISessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_SESSION_LIST);
        repository.add(USER2_SESSION_LIST);
        Assert.assertTrue(repository.existsById(USER1.getId(), USER1_SESSION1.getId()));
        Assert.assertFalse(repository.existsById(USER2.getId(), USER1_SESSION1.getId()));
    }

}
