package ru.t1.vsukhorukova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.sukhorukova.tm.api.repository.ISessionRepository;
import ru.t1.sukhorukova.tm.api.service.ISessionService;
import ru.t1.sukhorukova.tm.exception.entity.EntityNotFoundException;
import ru.t1.sukhorukova.tm.exception.entity.ValueIsNullException;
import ru.t1.sukhorukova.tm.exception.field.IdEmptyException;
import ru.t1.sukhorukova.tm.exception.field.IndexIncorrectException;
import ru.t1.sukhorukova.tm.exception.field.UserIdEmptyException;
import ru.t1.sukhorukova.tm.model.Session;
import ru.t1.sukhorukova.tm.repository.SessionRepository;
import ru.t1.sukhorukova.tm.service.SessionService;
import ru.t1.vsukhorukova.tm.marker.UnitCategory;

import java.util.List;

import static ru.t1.vsukhorukova.tm.constant.SessionTestData.*;
import static ru.t1.vsukhorukova.tm.constant.SessionTestData.USER1_SESSION1;
import static ru.t1.vsukhorukova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class SessionServiceTest {

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void check() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }

    @After
    public void clear() {
        sessionRepository.removeAll();
    }

    @Test
    public void testSessionAdd() {
        Assert.assertNotNull(sessionService.add(USER1_SESSION1));
        Assert.assertEquals(USER1_SESSION1, sessionRepository.findAll().get(0));

        thrown.expect(EntityNotFoundException.class);
        sessionService.add(NULL_SESSION);
    }

    @Test
    public void testSessionAddByUserId() {
        Assert.assertNotNull(sessionService.add(USER1.getId(), USER1_SESSION1));
        Assert.assertEquals(USER1_SESSION1, sessionRepository.findAll().get(0));

        thrown.expect(UserIdEmptyException.class);
        sessionService.add(NULL_USER_ID, USER1_SESSION1);
        thrown.expect(EntityNotFoundException.class);
        sessionService.add(USER1.getId(), NULL_SESSION);
    }

    @Test
    public void testSessionAddModels() {
        Assert.assertNotNull(sessionService.add(ADMIN_SESSION_LIST));
        Assert.assertEquals(ADMIN_SESSION_LIST.get(0), sessionRepository.findAll().get(0));
        Assert.assertEquals(ADMIN_SESSION_LIST.get(1), sessionRepository.findAll().get(1));

        thrown.expect(ValueIsNullException.class);
        sessionService.add(NULL_SESSION_LIST);
    }

    @Test
    public void testSessionSet() {
        Assert.assertNotNull(sessionService.set(ADMIN_SESSION_LIST));
        Assert.assertEquals(ADMIN_SESSION_LIST.get(0), sessionRepository.findAll().get(0));
        Assert.assertEquals(ADMIN_SESSION_LIST.get(1), sessionRepository.findAll().get(1));
        Assert.assertNotNull(sessionService.set(USER2_SESSION_LIST));
        Assert.assertEquals(USER2_SESSION_LIST.get(0), sessionRepository.findAll().get(0));

        thrown.expect(ValueIsNullException.class);
        sessionService.add(NULL_SESSION_LIST);
    }

    @Test
    public void testSessionFindAll() {
        sessionService.add(USER1_SESSION_LIST);
        @NotNull final List<Session> sessionList = sessionRepository.findAll();
        Assert.assertEquals(USER1_SESSION_LIST.get(0), sessionList.get(0));
        Assert.assertEquals(USER1_SESSION_LIST.get(1), sessionList.get(1));
        Assert.assertEquals(USER1_SESSION_LIST.get(2), sessionList.get(2));
    }

    @Test
    public void testSessionFindAllByUserId() {
        sessionRepository.add(USER1_SESSION_LIST);
        sessionRepository.add(ADMIN_SESSION_LIST);
        @NotNull final List<Session> sessionListUser1 = sessionService.findAll(USER1.getId());
        Assert.assertEquals(USER1_SESSION_LIST.get(0), sessionListUser1.get(0));
        Assert.assertEquals(USER1_SESSION_LIST.get(1), sessionListUser1.get(1));
        Assert.assertEquals(USER1_SESSION_LIST.get(2), sessionListUser1.get(2));
        @NotNull final List<Session> sessionListAdmin = sessionService.findAll(ADMIN.getId());
        Assert.assertEquals(ADMIN_SESSION_LIST.get(0), sessionListAdmin.get(0));
        Assert.assertEquals(ADMIN_SESSION_LIST.get(1), sessionListAdmin.get(1));

        thrown.expect(UserIdEmptyException.class);
        sessionService.findAll(NULL_USER_ID);
    }

    @Test
    public void testSessionFindOneById() {
        sessionRepository.add(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION2, sessionService.findOneById(USER1_SESSION2.getId()));
        Assert.assertNull(sessionService.findOneById("test-id"));

        thrown.expect(IdEmptyException.class);
        sessionService.findOneById(NULL_SESSION_ID);
    }

    @Test
    public void testSessionFindOneByIdByUserId() {
        sessionRepository.add(USER1_SESSION_LIST);
        sessionRepository.add(ADMIN_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION2, sessionService.findOneById(USER1.getId(), USER1_SESSION2.getId()));
        Assert.assertEquals(ADMIN_SESSION2, sessionService.findOneById(ADMIN.getId(), ADMIN_SESSION2.getId()));
        Assert.assertNull(sessionService.findOneById(USER1.getId(), ADMIN_SESSION2.getId()));
        Assert.assertNull(sessionService.findOneById(USER1.getId(), "test-id"));

        thrown.expect(UserIdEmptyException.class);
        sessionService.findOneById(NULL_USER_ID, ADMIN_SESSION2.getId());
        thrown.expect(IdEmptyException.class);
        sessionService.findOneById(NULL_SESSION_ID);
    }

    @Test
    public void testSessionFindOneByIndex() {
        sessionRepository.add(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION2, sessionService.findOneByIndex(1));

        thrown.expect(IndexIncorrectException.class);
        sessionService.findOneByIndex(-1);
    }

    @Test
    public void testSessionFindOneByIndexByUserId() {
        sessionRepository.add(USER1_SESSION_LIST);
        sessionRepository.add(ADMIN_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST.get(1), sessionService.findOneByIndex(USER1.getId(), 1));
        Assert.assertEquals(ADMIN_SESSION_LIST.get(0), sessionService.findOneByIndex(ADMIN.getId(), 0));

        thrown.expect(UserIdEmptyException.class);
        sessionService.findOneByIndex(NULL_USER_ID, 0);
        thrown.expect(IndexIncorrectException.class);
        sessionService.findOneByIndex(USER1.getId(), -1);
    }

    @Test
    public void testSessionRemoveOne() {
        sessionRepository.add(USER1_SESSION_LIST);
        Assert.assertNotNull(sessionRepository.findOneById(USER1_SESSION2.getId()));
        Assert.assertNotNull(sessionService.removeOne(USER1_SESSION2));
        Assert.assertNull(sessionRepository.findOneById(USER1_SESSION2.getId()));

        thrown.expect(EntityNotFoundException.class);
        sessionService.removeOne(NULL_SESSION);
    }

    @Test
    public void testSessionRemoveOneByUserId() {
        sessionRepository.add(USER1_SESSION_LIST);
        sessionRepository.add(USER2_SESSION_LIST);
        Assert.assertNotNull(sessionRepository.findOneById(USER1_SESSION2.getId()));
        sessionService.removeOne(USER2.getId(), USER1_SESSION2);
        Assert.assertNotNull(sessionRepository.findOneById(USER1_SESSION2.getId()));
        sessionService.removeOne(USER1.getId(), USER1_SESSION2);
        Assert.assertNull(sessionRepository.findOneById(USER1_SESSION2.getId()));

        thrown.expect(UserIdEmptyException.class);
        sessionService.removeOne(NULL_USER_ID, USER1_SESSION2);
        thrown.expect(EntityNotFoundException.class);
        sessionService.removeOne(USER1.getId(), NULL_SESSION);
    }

    @Test
    public void testSessionRemoveOneById() {
        sessionRepository.add(USER1_SESSION_LIST);
        Assert.assertNotNull(sessionRepository.findOneById(USER1_SESSION2.getId()));
        Assert.assertNotNull(sessionService.removeOneById(USER1_SESSION2.getId()));
        Assert.assertNull(sessionRepository.findOneById(USER1_SESSION2.getId()));

        thrown.expect(IdEmptyException.class);
        sessionService.removeOneById(NULL_SESSION_ID);
    }

    @Test
    public void testSessionRemoveOneByIdByUserId() {
        sessionRepository.add(USER1_SESSION_LIST);
        sessionRepository.add(USER2_SESSION_LIST);
        Assert.assertNotNull(sessionRepository.findOneById(USER1_SESSION2.getId()));
        Assert.assertNull(sessionService.removeOneById(USER2.getId(), USER1_SESSION2.getId()));
        Assert.assertNotNull(sessionRepository.findOneById(USER1_SESSION2.getId()));
        Assert.assertNotNull(sessionService.removeOneById(USER1.getId(), USER1_SESSION2.getId()));
        Assert.assertNull(sessionRepository.findOneById(USER1_SESSION2.getId()));

        thrown.expect(UserIdEmptyException.class);
        sessionService.removeOneById(NULL_USER_ID, USER1_SESSION2.getId());
        thrown.expect(IdEmptyException.class);
        sessionService.removeOneById(USER2.getId(), NULL_SESSION_ID);
    }

    @Test
    public void testSessionRemoveOneByIndex() {
        sessionRepository.add(USER1_SESSION1);
        Assert.assertNotNull(sessionRepository.findOneById(USER1_SESSION1.getId()));
        Assert.assertNotNull(sessionService.removeOneByIndex(0));
        Assert.assertNull(sessionRepository.findOneById(USER1_SESSION1.getId()));

        thrown.expect(IndexIncorrectException.class);
        sessionService.removeOneByIndex(-1);
    }

    @Test
    public void testSessionRemoveOneByIndexByUserId() {
        sessionRepository.add(USER1_SESSION1);
        sessionRepository.add(USER2_SESSION1);
        Assert.assertNotNull(sessionRepository.findOneById(USER1_SESSION1.getId()));
        Assert.assertNotNull(sessionRepository.findOneById(USER2_SESSION1.getId()));
        Assert.assertNotNull(sessionService.removeOneByIndex(USER1.getId(), 0));
        Assert.assertNotNull(sessionRepository.findOneById(USER2_SESSION1.getId()));
        Assert.assertNull(sessionRepository.findOneById(USER1_SESSION1.getId()));

        thrown.expect(UserIdEmptyException.class);
        sessionService.removeOneByIndex(NULL_USER_ID, 0);
        thrown.expect(IndexIncorrectException.class);
        sessionService.removeOneByIndex(USER1.getId(), -1);
    }

    @Test
    public void testSessionRemoveAll() {
        sessionRepository.add(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST.size(), sessionRepository.findAll().size());
        sessionService.removeAll();
        Assert.assertEquals(0, sessionRepository.findAll().size());
    }

    @Test
    public void testSessionRemoveAllByUserId() {
        sessionRepository.add(USER1_SESSION_LIST);
        sessionRepository.add(USER2_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST.size(), sessionRepository.findAll(USER1.getId()).size());
        Assert.assertEquals(USER2_SESSION_LIST.size(), sessionRepository.findAll(USER2.getId()).size());
        sessionService.removeAll(USER1.getId());
        Assert.assertEquals(0, sessionRepository.findAll(USER1.getId()).size());
        Assert.assertEquals(USER2_SESSION_LIST.size(), sessionRepository.findAll(USER2.getId()).size());

        thrown.expect(UserIdEmptyException.class);
        sessionService.removeAll(NULL_USER_ID);
    }

    @Test
    public void testSessionGetSize() {
        sessionRepository.add(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST.size(), sessionService.getSize());
    }

    @Test
    public void testSessionGetSizeByUserId() {
        sessionRepository.add(USER1_SESSION_LIST);
        sessionRepository.add(USER2_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST.size(), sessionService.getSize(USER1.getId()));
        Assert.assertEquals(USER2_SESSION_LIST.size(), sessionService.getSize(USER2.getId()));

        thrown.expect(UserIdEmptyException.class);
        sessionService.getSize(NULL_USER_ID);
    }

    @Test
    public void testSessionExistsById() {
        sessionRepository.add(USER1_SESSION_LIST);
        Assert.assertTrue(sessionService.existsById(USER1_SESSION1.getId()));
        Assert.assertFalse(sessionService.existsById(USER2_SESSION1.getId()));
    }

    @Test
    public void testSessionExistsByIdByUserId() {
        sessionRepository.add(USER1_SESSION_LIST);
        sessionRepository.add(USER2_SESSION_LIST);
        Assert.assertTrue(sessionService.existsById(USER1.getId(), USER1_SESSION1.getId()));
        Assert.assertFalse(sessionService.existsById(USER2.getId(), USER1_SESSION1.getId()));

        thrown.expect(UserIdEmptyException.class);
        sessionService.existsById(NULL_USER_ID, USER1_SESSION1.getId());
    }

}
